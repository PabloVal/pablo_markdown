# Esto va a ser la hoja de instalación
En relacion a los ejercicios de clase.


### Documentacion del punto f) del ejercicio

**Desde el git bash nos situamos en el directorio raiz y creamos una carpera nueva
mediante el comando mkdir**  
![](imagenes/CrearCarpeta.PNG "Carpeta")  
**Entramos dentro de la carpeta que acabamos de crear y añadimos/creamos el archivo
privado.txt mediante nano o touch**  
![](imagenes/CrearTxt.PNG "nano")  
**Podemos esccribir lo que queramos dentro , ya que el archivo no se va a subir al
repositorio remoto con lo que vamos a hacer en el siguiente punto.**  


### A continuación documentamos el punto g) del ejercicio:    


**Lo primero que hacemos es crear un archivo .gitignore**  
 ![](imagenes/nano.PNG "nano")  
 **Subimos el archivo git , junto con otros archivos e imagenes 
 que podamos haber añadido al repositorio local**  
 ![](imagenes/add_archivo.PNG "add")  
Despues debemos escribir dentro de el lo siguiente:  
 ![](imagenes/ignorado.PNG "add")  
 **Ponemos los archivos o carpetas que queremos que sean ignorados y no se suban
 al repositorio remoto, despues hacemos un git add. y  un commit y lo subimos**

### Documentacion del punto i)
**Añadimos un fichero con nuestro nombre.md en el que añadimos los modulos que 
estamos cursando**  
![](imagenes/fichero pablo.PNG "nano")  

### Documentacion del punto j)
**Debemos crear un tag mediante el comando git tag**  
![](imagenes/tag.PNG "nano")  

# Documentacion de Hoja_03

**Lo priemro que hacemos es crear una rama branch con mi nombre mediante el comando
git branch**  
**Después nos moveremos a la rama que hemos creado , para ello tenemos que usar el comando git checkout "nombre de la rama"**
![](imagenes/branch.PNG "nano")  
**Continuamos con la creacion de un archivo llamado daw.md que crearemos escribiendo
touch daw.md y dentro del archivo una cabezera con el texto "DESARROLLO DE
APLICACIONES WEB"**  
![](imagenes/daw.PNG "nano")  
**Tras crear todo hacemos un commit y lo subimos con git push origin-ramaTuNombre**  
![](commit_rama.PNG "nano")  
**Editamos el fichero daw.md de la carpeta master con los modulos que cursamos el primer curso de DAW y luego en la rama-TuNombre los modulos que damos en el segundo año de DAW.**  
![](imagenes/añadiendoTabla2.PNG "nano")
**Para despues nos posicionamos en la rama master y ejecutamos un merge rama-TuNombre y vemos que pasa**  
1.  Comenzamos a arreglar el conflicto
    * ![](imagenes/conflicto.PNG "nano")  
    * Ocurre un conflicto que debemos solucionar.Archivo del conflicto:  
        ![](imagenes/archivo_conflicto.PNG "nano")  
    * Conflicto resuelto:  
        ![](imagenes/archivo_arreglado.PNG "nano")  

**Por ultimo creamos un tag nuevo de la version 0.2(v0.2) y borramos la rama rama-TuNombre porque no la vamos a utilizar más:**  
![](imagenes/tag_deleteBranch.PNG "nano")  








